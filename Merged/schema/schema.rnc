default namespace = ""
namespace html = "https://www.w3schools.com/html/"

start =
element entities{
    element entity {
      attribute type { xsd:NCName },
      element name{text},
      element flagImg {
        attribute href { text }
      }?,
      element map {
        attribute href { text }
      },
      element photos {
        element photo {
          attribute href { text }
        }+
      },
      element introduction {
        element background { html.p+ }
      },
      element geography {
        element location { text },
        geographicCoordinates,
        element mapReferences { text },
        element area {
          (total,
          attribute worldComparison { text },
          notes?)|
          (total,
          attribute worldComparison { text },
          element land { text },
          element water { text },
          notes?)
        },
        element areaComparative { text },
        element landBoundaries {
          total,
          element borderCountries {
            element country {
              attribute length { text },
              text
            }+
          },
          notes?
        }?,
        element coastline { text },
        element maritimeClaims {
          notes?,
          element territorialSea { text }?,
          element contiguousZone { text }?,
          element exclusiveEconomicZone { text }?,
          element continentalShelf { text }?
        }?,
        element climate {
          element climateOverview { text },
          notes?
        },
        element terrain {
          element terrainOverview { text },
          element majorSurfaceCurrents { text }?
        },
        element elevation {
          element highestPoint {
            attribute height { text },
            text
          },
          element lowestPoint {
            attribute height { text },
            text
          },
          (element meanElevation {
            attribute height { text }
          } |
          element meanDepth{
            attribute height { text }
          }),
          element oceanZones { html.p+ }?,
          notes?
        },
        element naturalResources { values, notes? },
        element irrigatedLand { values }?,
        element populationDistribution { text }?,
        element naturalHazards { text }?,
        notes?
      },
      element environment {
        element currentIssues { text }|
        (element currentIssues { text },
        element marineFisheries { text }?,
        element internationalAgreements {
          element partyTo { values },
          element signedNotRatified { values|text }
        },
        element airPollutants {
          element emissions {
            element particulateMatter {
              attribute year { text },
              text
            },
            element carbonDioxide {
              attribute year { text },
              text
            },
            element methane {
              attribute year { text },
              text
            }
          }
        },
        element totalWaterWithdrawal {
          element municipal {
            attribute year { text },
            text
          },
          element industrial {
            attribute year { text },
            text
          },
          element agricultural {
            attribute year { text },
            text
          }
        },
        element totalRenewableWaterResources { values },
        element landUse {
          element agriculturalLand {
            attribute year { text },
            text
          },
          element arableLand {
            attribute year { text },
            text
          },
          element permanentCrops {
            attribute year { text },
            text
          },
          element permanentPasture {
            attribute year { text },
            text
          },
          element forest {
            attribute year { text },
            text
          },
          other
        },
        element revenue {
          element fromForestResources {
            attribute worldComparison { text },
            values
          },
          element fromCoal {
            attribute worldComparison { text },
            values
          }
        },
        element urbanization {
          element population {
            attribute year { text },
            text
          },
          element rate {
            attribute year { text },
            text
          }
        },
        element majorInfectiousDiseases {
          element degreeOfRisk {
            attribute year { text },
            text
          },
          element diseases {
            element foodOrWaterborne { values },
            element vectorborne { values }
          }
        }?,
        element foodInsecurity { text }?,
        element wasteAndRecycling {
          element municipalSolidWasteGeneratedAnnually {
            attribute year { text },
            text
          },
          element municipalSolidWasteRecycledAnnually {
            attribute year { text },
            text
          }?,
          element percentOfMunicipalSolidWasteRecycled {
            attribute year { text },
            text
          }?
        })
      },
      element government {
        countryName|
        (countryName,
        element governmentType { text },
        element capital {
          name,
          geographicCoordinates,
          element timeDifference { text },
          element daylightSavingTime { text },
          etymology,
          notes?
        },
        element administrativeDivisions {
          element summary { text },
          values,
          notes?
        },
        element dependentAreas { values, notes? }?,
        element independence { text },
        element nationalHoliday { text },
        element constitution {
          element history { text },
          element amendments { text }
        },
        element legalSystem { text },
        element internationalLawOrganizationParticipation { text },
        element citizenship {
          element byBirth { text },
          element byDescentOnly { text },
          element dualCitizenshipRecognized { text },
          element residencyRequirementForNaturalization { text }
        },
        element suffrage { text },
        element executiveBranch {
          element chiefOfState { text },
          element headOfGovernment { text },
          element cabinet { text },
          element electionsAndAppointments { text },
          electionResults
        },
        element legislativeBranch {
          description,
          element elections { text },
          electionResults,
          notes?
        },
        element judicialBranch {
          element highestCourts { text },
          element judgeSelectionAndTermOfOffice { text },
          element subordinateCourts { text },
          notes?
        },
        element politicalPartiesAndLeaders { text },
        element internationalOrganizationParticipation { values },
        element diplomaticRepresentationInTheUS {
          chiefOfMission,
          element chancery { text },
          telephone,
          FAX,
          element consulatesGeneral { values }
        }?,
        element diplomaticRepresentationFromTheUS {
          chiefOfMission,
          telephone,
          element embassy { text },
          element mailingAddress { text },
          FAX
        }?,
        element flag { description, notes? },
        element nationalSymbols { text },
        element nationalAnthem {
          name?,
          element lyrics { text },
          element music { text },
          notes?,
          element link {
            attribute href { text }
          }
        })
      },
      element economy {
        element economyOverview { html.p+ }|
        (element economyOverview { html.p+ },
        element GDP {
          element realGrowthRate {
            attribute worldComparison { text },
            values
          },
          element realPurchasingPowerParity {
            attribute worldComparison { text },
            values,
            notes?
          },
          element officialExchangeRate { values },
          element realPerCapita {
            attribute worldComparison { text },
            values,
            notes?
          },
          element compositionBySectorOfOrigin {
            element agriculture {
              attribute year { text },
              text
            },
            element industry {
              attribute year { text },
              text
            },
            element services {
              attribute year { text },
              text
            },
            notes?
          },
          element compositionByEndUse {
            element householdConsumption {
              attribute year { text },
              text
            },
            element governmentConsumption {
              attribute year { text },
              text
            },
            element investmentInFixedCapital {
              attribute year { text },
              text
            },
            element investmentInInventories {
              attribute year { text },
              text
            },
            element exportsOfGoodsAndServices {
              attribute year { text },
              text
            },
            element importsOfGoodsAndServices {
              attribute year { text },
              text
            }
          }
        },
        element inflationRateConsumerPrices {
          attribute worldComparison { text },
          values
        },
        element creditRatings {
          element fitchRating {
            attribute year { text },
            text
          },
          element moodysRating {
            attribute year { text },
            text
          },
          element standardPoorsRating {
            attribute year { text },
            text
          }
        }?,
        element grossNationalSaving {
          attribute worldComparison { text },
          values
        },
        element easeOfDoingBusinessIndexScores {
          element overallScore {
            attribute year { text },
            text
          },
          element startingBusinessScore {
            attribute year { text },
            text
          },
          element tradingScore {
            attribute year { text },
            text
          },
          element enforcementScore {
            attribute year { text },
            text
          }
        },
        element agriculturalProducts { values },
        element industries { values },
        element industrialProductionGrowthRate {
          attribute worldComparison { text },
          values
        },
        element laborForce {
          attribute worldComparison { text },
          values,
          notes?
        },
        element laborForceByOccupation {
          element occupations {
            element occupation {
              attribute name { text },
              attribute year { text }?,
              text
            }+
          },
          notes?
        },
        element unemploymentRate {
          attribute worldComparison { text },
          values
        },
        element populationBelowPovertyLine { values },
        element giniIndexCoefficientDistributionOfFamilyIncome {
          attribute worldComparison { text },
          values
        },
        element householdIncomeOrConsumptionByPercentageShare {
          element lowest10 { text },
          element highest10 {
            attribute year { text },
            text
          }
        },
        element budget {
          element revenues {
            attribute year { text },
            text
          },
          element expenditures {
            attribute year { text },
            text
          },
          notes?
        },
        element taxesAndOtherRevenues {
          attribute worldComparison { text },
          values,
          notes?
        },
        element budgetSurplusOrDeficit {
          attribute worldComparison { text },
          values
        },
        element publicDebt {
          attribute worldComparison { text },
          values,
          notes?
        },
        element fiscalYear { text },
        element currentAccountBalance {
          attribute worldComparison { text },
          values
        },
        element exports {
          element exportsOverview {
            attribute worldComparison { text },
            values,
            notes?
          },
          partners,
          commodities
        },
        element imports {
          element importsOverview {
            attribute worldComparison { text },
            values
          },
          partners,
          commodities
        },
        element reservesOfForeignExchangeAndGold {
          attribute worldComparison { text },
          values
        },
        element debtExternal {
          attribute worldComparison { text },
          values,
          notes?
        },
        element exchangeRates {
          element currencyName { text },
          element rates {
            element currency {
              attribute name { text },
              value+
            }+
          },
          notes?
        })
      },
      element transportation {
        (portsAndTerminals,notes?)|
        (element nationalAirTransportSystem {
          element registeredAirCarriers {
            attribute year { text },
            text
          },
          element inventoryOfRegisteredAircraftOperatedByAirCarriers {
            text
          },
          element annualPassengerTrafficOnRegisteredAirCarriers {
            attribute year { text },
            text
          },
          element annualFreightTrafficOnRegisteredAirCarriers {
            attribute year { text },
            text
          }
        },
        element civilAircraftRegistrationCountryCodePrefix { text },
        element airports {
          element airportsOverview {
            attribute worldComparison { text },
            total
          },
          element withPavedRunways { total, values },
          element withUnpavedRunways { total, values }
        },
        element heliports { values },
        element pipelines { values },
        element railways {
          attribute worldComparison { text },
          total,
          element standardGauge {
            attribute year { text },
            text
          },
          element narrowGauge {
            attribute year { text },
            text
          }?,
          other?
        }?,
        element roadways {
          attribute worldComparison { text },
          total,
          element paved {
            attribute year { text },
            text
          },
          element unpaved {
            attribute year { text },
            text
          }?
        },
        element waterways {
          attribute worldComparison { text },
          values
        },
        element merchantMarine {
          attribute worldComparison { text },
          total,
          element byType {
            attribute year { text },
            text
          }
        }?,
        portsAndTerminals,notes?)
      }
    }+
  }
html.p = element html:p { text }
geographicCoordinates = element geographicCoordinates { text }
total =
  element total {
    attribute year { text }?,
    text?
  }
notes =
  element notes {
    element note { text }+
  }
values = element values { value+ }
other = element other { values }
etymology = element etymology { text }
name = element name { text }
electionResults = element electionResults { text }
description = element description { text }
chiefOfMission = element chiefOfMission { text }
telephone = element telephone { text }
FAX = element FAX { text }
partners =
  element partners {
    attribute year { text },
    values
  }
commodities =
  element commodities {
    attribute year { text },
    values
  }
value =
  element value {
    attribute TEU { text }?,
    attribute country { text }?,
    attribute date { text }?,
    attribute lenght { text }?,
    attribute passengers { text }?,
    attribute percentage { text }?,
    attribute year { text }?,
    text
  }
countryName =  
  element countryName {
    element forms {
      element conventionalLong { text },
      element conventionalShort { text },
      element localLong { text }?,
      element localShort { text }?
    }?,
    element abbreviation { text }?,
    element former { text }?,
    etymology
  }
portsAndTerminals =
  element portsAndTerminals {
    element majorSeaports { values }?,
    element oilTerminals { values }?,
    element containerPorts {
      attribute year { text },
      values
    }?,
    element LNGTerminals {
      element export { values, notes? },
      element import { values }
    }?,
    element cargoPorts { values }?,
    element cruiseDeparturePorts { values }?,
    element riverPorts { values }?
  }