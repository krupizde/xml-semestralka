<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:output method="html" />
    <xsl:template match="entity">
        <xsl:result-document href="./html/{name}.html" method="html">
            <html>
                <head>
                    <meta charset="UTF-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <link rel="stylesheet" href="styles.css" />
                    <title>
                        <xsl:value-of select="name" />
                    </title>
                </head>
                <body>
                    <xsl:call-template name="navbar"></xsl:call-template>
                    <main>
                        <h1>
                            <xsl:value-of select="@type" />
                            -
                            <xsl:value-of select="name" />
                        </h1>
                        <section id="imgs">
                            <xsl:if test="flagImg">
                                <img src="{flagImg/@href}" alt="flag" />
                            </xsl:if>
                            <img src="{map/@href}" alt="map" />

                        </section>
                        <section>
                            <h2>
                                <a href="{name}Galery.html">Photos</a>
                            </h2>
                            <xsl:apply-templates select="photos" />
                        </section>
                        <section class="bigSection" id="introductionSection">
                            <xsl:apply-templates select="introduction" />
                        </section>

                        <section class="bigSection" id="geographySection">
                            <xsl:apply-templates select="geography" />
                        </section>

                        <section class="bigSection" id="environmentSection">
                            <xsl:apply-templates select="environment" />
                        </section>

                        <section class="bigSection" id="governmentSection">
                            <xsl:apply-templates select="government" />
                        </section>

                        <section class="bigSection" id="economySection">
                            <xsl:apply-templates select="economy" />
                        </section>

                        <section class="bigSection" id="transportationSection">
                            <xsl:apply-templates select="transportation" />
                        </section>
                    </main>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    <xsl:template name="navbar">
        <nav id="navbar">
            <h1>
                <a href="index.html">Back</a>
            </h1>
            <ul>
                <xsl:for-each select="*[not(name()='name') and not(name()='flagImg') and not(name()='map') and not(name()='photos')]">
                    <li>
                        <a href="#{name()}">
                            <xsl:value-of select="name()" />
                        </a>
                    </li>
                </xsl:for-each>
            </ul>
        </nav>
    </xsl:template>
    <xsl:template match="entity/photos">
        <xsl:result-document href="./html/{../name}Galery.html" method="html">
            <html>
                <head>
                    <meta charset="UTF-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <title>
                        <xsl:value-of select="name" />
                        Galery
                    </title>
                </head>
                <body>
                    <h2>
                        <a href="{../name}.html">Back</a>
                    </h2>
                    <ul>
                        <xsl:for-each select="photo">
                            <li>
                                <img src="{@href}"></img>
                            </li>
                        </xsl:for-each>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="entity/*[not(name()='photos')]">
        <h2>
            <xsl:attribute name="id">
                <xsl:value-of select="name()" />
            </xsl:attribute>
            <xsl:call-template name="breakIntoWords">
                <xsl:with-param name="string" select="name()" />
            </xsl:call-template>
        </h2>
        <xsl:apply-templates select="./*" />
    </xsl:template>

    <xsl:template match="background | economyOverview">
        <section>
            <h3>
                <xsl:attribute name="id">
                    <xsl:value-of select="name()" />
                </xsl:attribute>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
            </h3>
            <xsl:for-each select="./p">
                <p>
                    <xsl:value-of select="." />
                </p>
            </xsl:for-each>
        </section>
    </xsl:template>
    <xsl:template match="oceanZones">
        <section>
            <xsl:attribute name="id">
                <xsl:value-of select="name()" />
            </xsl:attribute>
            <h4>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
                :
            </h4>
            <xsl:for-each select="./p">
                <p>
                    <xsl:value-of select="." />
                </p>
            </xsl:for-each>
        </section>
    </xsl:template>

    <xsl:template match="exchangeRates/rates">
        <section id="rates">
            <xsl:for-each select="currency">
                <h5>
                    Per
                    <xsl:value-of select="@name" />:
                </h5>
                <ul>
                    <xsl:for-each select="value">
                        <li>
                            <xsl:value-of select="." />
                            (
                            <xsl:value-of select="@year" />
                            )
                        </li>
                    </xsl:for-each>
                </ul>
            </xsl:for-each>
        </section>
    </xsl:template>

    <xsl:template match="geography/*[not(*)] | environment/*[not(*)] | government/*[not(*)] | economy/*[not(*)] | transportation/*[not(*)] ">
        <section>
            <h3>
                <xsl:attribute name="id">
                    <xsl:value-of select="name()" />
                </xsl:attribute>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
            </h3>
            <p>
                <xsl:value-of select="." />
                <xsl:if test="@year">
                    (
                    <xsl:value-of select="@year" />
                    )
                </xsl:if>
            </p>
            <xsl:if test="@worldComparison">
                World comparison:
                <xsl:value-of select="@worldComparison" />
            </xsl:if>
        </section>
    </xsl:template>
    <xsl:template match="geography/*[* and not(name()='notes')] | environment/*[* and not(name()='notes')] |
     government/*[* and not(name()='notes')] | economy/*[* and not(name()='notes') and not(name()='economyOverview')] | transportation/*[* and not(name()='notes')] ">
        <section>
            <h3>
                <xsl:attribute name="id">
                    <xsl:value-of select="name()" />
                </xsl:attribute>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
            </h3>
            <xsl:apply-templates select="*" />
            <xsl:if test="@worldComparison">
                <p>
                    World comparison:
                    <xsl:value-of select="@worldComparison" />
                </p>
            </xsl:if>
        </section>
    </xsl:template>
    <xsl:template match="occupations">
        <ul>
            <xsl:for-each select="occupation">
                <li>
                    <b>
                        <xsl:value-of select="@name" />
                    </b>
                    :
                    <xsl:value-of select="." />
                    <xsl:if test="@year">
                        (
                        <xsl:value-of select="@year" />
                        )
                    </xsl:if>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template match="forms">
        <xsl:for-each select="*">
            <p>
                <b>
                    <xsl:call-template name="breakIntoWords">
                        <xsl:with-param name="string" select="name()" />
                    </xsl:call-template>
                    Form
                </b>
                :
                <xsl:value-of select="." />
            </p>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="notes">
        <section>
            <h4>Notes</h4>
            <ul class="notes">
                <xsl:for-each select="note">
                    <li>
                        <b>Note</b>
                        :
                        <xsl:value-of select="." />
                    </li>
                </xsl:for-each>
            </ul>
        </section>
    </xsl:template>
    <xsl:template match="borderCountries">
        <ul>
            <xsl:for-each select="country">
                <li>
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@length" />
                    )
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template match="partyTo | signedNotRatified | emissions | diseases | consulatesGeneral | portsAndTerminals/* | airports/*">
        <section>
            <h4>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
                :
            </h4>
            <xsl:apply-templates select="*" />
        </section>
    </xsl:template>
    <xsl:template match="GDP/*">
        <section>
            <h4>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
            </h4>
            <xsl:apply-templates select="*" />
            <xsl:if test="@worldComparison">
                <p>
                    World comparison:
                    <xsl:value-of select="@worldComparison" />

                </p>
            </xsl:if>
        </section>
    </xsl:template>
    <xsl:template match="foodOrWaterborne | vectorborne">
        <section>
            <h5>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
            </h5>
            <xsl:apply-templates select="*" />
        </section>
    </xsl:template>
    <xsl:template match="revenue/* ">
        <section>
            <h4>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
            </h4>
            <xsl:apply-templates select="*" />
        </section>
    </xsl:template>
    <xsl:template match="entity/*/*/*[not(*) and not(name() = 'note') and not(name() = value) and not(name() = 'country') and not(name() = 'p') and not(name() = 'link')] |
    entity/*/*/*/*[not(*) and not(name() = 'note') and not(name() = value) and not(name() = 'country') and not(name() = 'p') and not(name() = 'link')] |
    entity/*/*/*/*/*[not(*) and not(name() = 'note') and not(name() = value) and not(name() = 'country') and not(name() = 'p') and not(name() = 'link')]">
        <p>
            <b>
                <xsl:call-template name="breakIntoWords">
                    <xsl:with-param name="string" select="name()" />
                </xsl:call-template>
                :
            </b>
            <xsl:value-of select="." />
            <xsl:if test="@year">
                (
                <xsl:value-of select="@year" />
                )
            </xsl:if>
            <xsl:if test="@height">
                (
                <xsl:value-of select="@height" />
                )
            </xsl:if>
        </p>
        <xsl:if test="@worldComparison">
            <p>
                World comparison:
                <xsl:value-of select="@worldComparison" />
            </p>
        </xsl:if>
    </xsl:template>
    <xsl:template match="link">
        <audio controls="true">
            <source src="{@href}" type="audio/mp3" />
            Your browser does not support the audio element.
        </audio>
    </xsl:template>
    <xsl:template match="values" name="values">
        <ul>
            <xsl:for-each select="value[@year]">
                <li>
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@year" />
                    )
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[@country]">
                <li>
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@country" />
                    )
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[@date]">
                <li>
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@date" />
                    )
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[@lenght]">
                <li>
                    <b>
                        <xsl:value-of select="@lenght" />
                    </b>
                    :
                    <xsl:value-of select="." />
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[@passengers]">
                <li>
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@passengers" />
                    )
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[@percentage]">
                <li>
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@percentage" />
                    %
                    )
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[@name and @year]">
                <li>
                    <b>
                        <xsl:value-of select="@name" />
                    </b>
                    :
                    <xsl:value-of select="." />
                    (
                    <xsl:value-of select="@year" />
                    )
                </li>
            </xsl:for-each>
            <xsl:for-each select="value[not(@name or @year or @percentage or @passengers or @lenght or @country or @date)]">
                <li>
                    <xsl:value-of select="." />
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template match="exports/exportsOverview | imports/importsOverview">
        <section>
            <h4>Overview</h4>
            <xsl:apply-templates select="*" />
            <p>
                World comparison:
                <xsl:value-of select="@worldComparison" />
            </p>
        </section>
    </xsl:template>
    <xsl:template match="partners">
        <section>
            <h4>Partners</h4>
            <xsl:apply-templates select="*" />
            <p>
                (
                <xsl:value-of select="@year" />
                )
            </p>
        </section>
    </xsl:template>
    <xsl:template match="commodities">
        <section>
            <h4>Commodities</h4>
            <xsl:apply-templates select="*" />
            <p>
                (
                <xsl:value-of select="@year" />
                )
            </p>
        </section>
    </xsl:template>
    <xsl:template name="breakIntoWords">
        <xsl:param name="string" />
        <xsl:choose>
            <xsl:when test="string-length($string) &lt; 5">
                <xsl:value-of select="$string" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="breakIntoWordsHelper">
                    <xsl:with-param name="string" select="$string" />
                    <xsl:with-param name="token" select="substring($string, 1, 1)" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="breakIntoWordsHelper">
        <xsl:param name="string" select="''" />
        <xsl:param name="token" select="''" />
        <xsl:choose>
            <xsl:when test="string-length($string) = 0" />
            <xsl:when test="string-length($token) = 0" />
            <xsl:when test="string-length($string) = string-length($token)">
                <xsl:value-of select="$token" />
            </xsl:when>
            <xsl:when test="contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ',substring($string, string-length($token) + 1, 1))">
                <xsl:value-of select="concat($token, ' ')" />
                <xsl:call-template name="breakIntoWordsHelper">
                    <xsl:with-param name="string" select="substring-after($string, $token)" />
                    <xsl:with-param name="token" select="substring($string, string-length($token), 1)" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="breakIntoWordsHelper">
                    <xsl:with-param name="string" select="$string" />
                    <xsl:with-param name="token" select="substring($string, 1, string-length($token) + 1)" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>