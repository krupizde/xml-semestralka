<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:output method="xml" encoding="utf-8" />

  <xsl:template match="/">
    <fo:root font-family="Times New Roman, serif" font-size="11pt" language="cs" hyphenate="true">
      <fo:layout-master-set>
        <fo:simple-page-master master-name="my-page" page-height="297mm" page-width="210mm" margin="1in">
          <fo:region-body margin-bottom="10mm" margin-top="10mm" />
          <fo:region-before extent="5mm" />
          <fo:region-after extent="5mm" />
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="my-page">
        <fo:static-content flow-name="xsl-region-after">
          <fo:block>
            <xsl:text>Strana </xsl:text>
            <fo:page-number />
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="xsl-region-before">
          <fo:block text-align="right">
            <fo:retrieve-marker retrieve-class-name="kapitola" retrieve-position="first-including-carryover" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:list-block font-size="35pt" start-indent="0pt" font-weight="bold" space-after="200pt" text-align="left">
            <xsl:for-each select="entities/*">
              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block></fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="10pt">
                  <fo:block text-decoration="underline">
                    <fo:basic-link internal-destination="{name}">
                      <xsl:value-of select="name" />
                    </fo:basic-link>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </xsl:for-each>
          </fo:list-block>
          <xsl:apply-templates select="*" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template match="entity">
    <fo:block text-align="left" font-size="30pt" font-weight="bold" space-before="0pt" space-after="10pt" id="{name}">
      <xsl:value-of select="name" />
      <fo:list-block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
        <xsl:for-each select="*[not(name()='name')]">
          <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
              <fo:block></fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="10pt">
              <fo:block font-size="20pt" text-decoration="underline">
                <fo:basic-link internal-destination="{concat(../name,name())}">
                  <xsl:value-of select="name()" />
                </fo:basic-link>
              </fo:block>
            </fo:list-item-body>
          </fo:list-item>
        </xsl:for-each>
      </fo:list-block>
    </fo:block>
    <xsl:apply-templates select="./*" />
  </xsl:template>

  <xsl:template match="entity/*[not(name()='name') and not(name()='photos') and not(name()='flagImg') and not(name()='map')]">
    <fo:block text-align="left" font-size="24pt" font-weight="bold" space-before="6pt" space-after="4pt" id="{concat(../name,name())}">
      <fo:marker marker-class-name="kapitola">
        <xsl:value-of select="../name" />
        -
        <xsl:value-of select="name()" />
      </fo:marker>
      <xsl:call-template name="breakIntoWords">
        <xsl:with-param name="string" select="name()" />
      </xsl:call-template>
      <xsl:apply-templates select="*" />
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/*/*[* and not(name()='notes') and not(name()='economyOverview') and not(name()='background')]">
    <fo:block font-size="20pt" start-indent="17pt" text-align="left" space-after="4pt">
      <xsl:call-template name="breakIntoWords">
        <xsl:with-param name="string" select="name()" />
      </xsl:call-template>
      <xsl:apply-templates select="*" />
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/photos">
    <fo:block id="{concat(../name,name())}">
      <fo:marker marker-class-name="kapitola">
        <xsl:value-of select="../name" />
        -
        <xsl:value-of select="name()" />
      </fo:marker>
      <fo:inline text-align="left" font-size="24pt" font-weight="bold" space-before="6pt" space-after="4pt">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
      </fo:inline>
      <fo:list-block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
        <xsl:for-each select="photo">
          <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
              <fo:block></fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="0pt">
              <fo:block>
                <fo:external-graphic src="{@href}" content-height="scale-to-fit" height="2.50in" content-width="3.50in" />
              </fo:block>
            </fo:list-item-body>
          </fo:list-item>
        </xsl:for-each>
      </fo:list-block>
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/flagImg | entity/map">
    <fo:block id="{concat(../name,name())}">
      <fo:marker marker-class-name="kapitola">
        <xsl:value-of select="../name" />
        -
        <xsl:value-of select="name()" />
      </fo:marker>
      <fo:block text-align="left" font-size="24pt" font-weight="bold" space-before="6pt" space-after="4pt">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
      </fo:block>
      <fo:external-graphic src="{@href}" content-height="scale-to-fit" height="2.00in" content-width="2.00in" />
    </fo:block>
  </xsl:template>

  <xsl:template match="background | economyOverview">
    <fo:block font-size="15pt" start-indent="27pt" font-weight="normal" text-align="left">
      <fo:block font-size="20pt" start-indent="17pt" font-weight="bold" text-align="left">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
      </fo:block>
      <xsl:for-each select="./p">
        <fo:block font-size="15pt" start-indent="27pt" text-align="left">
          <xsl:value-of select="." />
        </fo:block>
      </xsl:for-each>
    </fo:block>
  </xsl:template>
  <xsl:template match="oceanZones">
    <fo:block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
      <fo:block font-size="15pt" start-indent="27pt" font-weight="bold" text-align="left">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
        :
      </fo:block>
      <xsl:for-each select="./p">
        <fo:block font-size="15pt" start-indent="35pt" text-align="left">
          <xsl:value-of select="." />
        </fo:block>
      </xsl:for-each>
    </fo:block>
  </xsl:template>
  <xsl:template match="occupations">
    <fo:list-block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
      <xsl:for-each select="occupation">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <fo:inline font-weight="bold">
                <xsl:value-of select="@name" />
                :
              </fo:inline>
              <xsl:value-of select="." />
              <xsl:if test="@year">
                (
                <xsl:value-of select="@year" />
                )
              </xsl:if>
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
    </fo:list-block>
  </xsl:template>
  <xsl:template match="exchangeRates/rates">
    <fo:block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
      <xsl:for-each select="currency">
        <fo:inline font-weight="bold">
          Per
          <xsl:value-of select="@name" />
          :
        </fo:inline>
        <fo:list-block font-size="15pt" start-indent="42pt" font-weight="normal" text-align="left">
          <xsl:for-each select="value">
            <fo:list-item>
              <fo:list-item-label end-indent="label-end()">
                <fo:block>-</fo:block>
              </fo:list-item-label>
              <fo:list-item-body start-indent="body-start()">
                <fo:block>
                  <xsl:value-of select="." />
                  (
                  <xsl:value-of select="@year" />
                  )
                </fo:block>
              </fo:list-item-body>
            </fo:list-item>
          </xsl:for-each>
        </fo:list-block>
      </xsl:for-each>
    </fo:block>
  </xsl:template>

  <xsl:template match="forms">
    <xsl:for-each select="*">
      <fo:block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
        <fo:inline font-weight="bold">
          <xsl:call-template name="breakIntoWords">
            <xsl:with-param name="string" select="name()" />
          </xsl:call-template>
          Form:
        </fo:inline>
        <xsl:value-of select="." />
      </fo:block>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="borderCountries">
    <fo:list-block font-size="15pt" start-indent="42pt" font-weight="normal" text-align="left">
      <xsl:for-each select="country">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
              (
              <xsl:value-of select="@length" />
              )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
    </fo:list-block>
  </xsl:template>
  <xsl:template match="partyTo | signedNotRatified | emissions | diseases | consulatesGeneral | portsAndTerminals/* | airports/* | landUse/other">
    <fo:block font-size="15pt" start-indent="42pt" font-weight="normal" text-align="left">
      <fo:inline font-weight="bold">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
        :
      </fo:inline>
      <xsl:apply-templates select="*" />
    </fo:block>
  </xsl:template>
  <xsl:template match="GDP/*">
    <fo:block font-size="15pt" start-indent="35pt" font-weight="normal" text-align="left">
      <fo:inline font-weight="bold">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
      </fo:inline>
      <xsl:apply-templates select="*" />
      <xsl:if test="@worldComparison">
        <fo:inline font-weight="lighter" font-size="10pt">
          World comparison:
          <xsl:value-of select="@worldComparison" />
        </fo:inline>
      </xsl:if>
    </fo:block>
  </xsl:template>
  <xsl:template match="foodOrWaterborne | vectorborne">
    <fo:block font-size="13pt" start-indent="47pt" font-weight="normal" text-align="left">
      <fo:inline font-weight="bold">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
      </fo:inline>
      <xsl:apply-templates select="*" />
    </fo:block>
  </xsl:template>
  <xsl:template match="revenue/* ">
    <fo:block font-size="15pt" start-indent="35pt" text-align="left">
      <fo:inline font-weight="bold">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
      </fo:inline>
      <xsl:apply-templates select="*" />
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/*/*[not(*)]">
    <fo:block font-size="20pt" start-indent="17pt" text-align="left">
      <xsl:call-template name="breakIntoWords">
        <xsl:with-param name="string" select="name()" />
      </xsl:call-template>
      <fo:block font-size="15pt" font-weight="normal" start-indent="27pt" text-align="left">
        <xsl:value-of select="." />
      </fo:block>
    </fo:block>
  </xsl:template>

  <xsl:template match="notes">
    <fo:block start-indent="27pt" font-size="15pt" font-weight="normal">
      <fo:block start-indent="24pt" font-size="15pt" font-weight="bold">Notes</fo:block>
      <fo:list-block>
        <xsl:for-each select="note">
          <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
              <fo:block>-</fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="body-start()">
              <fo:block>
                <fo:inline font-weight="bold">Note: </fo:inline>
                <xsl:value-of select="." />
              </fo:block>
            </fo:list-item-body>
          </fo:list-item>
        </xsl:for-each>
      </fo:list-block>
    </fo:block>
  </xsl:template>

  <xsl:template match="exports/exportsOverview | imports/importsOverview">
    <fo:block font-size="15pt" start-indent="27pt" font-weight="normal" text-align="left">
      <fo:block start-indent="24pt" font-size="15pt" font-weight="bold">Overview</fo:block>
      <xsl:apply-templates select="*" />
      <fo:inline font-weight="lighter" font-size="10pt">
        World comparison:
        <xsl:value-of select="@worldComparison" />
      </fo:inline>
    </fo:block>
  </xsl:template>
  <xsl:template match="partners">
    <fo:block font-size="15pt" start-indent="27pt" font-weight="normal" text-align="left" space-before="7pt">
      <fo:block start-indent="24pt" font-size="15pt" font-weight="bold">Partners</fo:block>
      <xsl:apply-templates select="*" />
      <fo:block>
        (
        <xsl:value-of select="@year" />
        )
      </fo:block>
    </fo:block>
  </xsl:template>
  <xsl:template match="commodities">
    <fo:block font-size="15pt" start-indent="27pt" font-weight="normal" text-align="left" space-before="7pt">
      <fo:block start-indent="24pt" font-size="15pt" font-weight="bold">Commodities</fo:block>
      <xsl:apply-templates select="*" />
      <fo:block>
        (
        <xsl:value-of select="@year" />
        )
      </fo:block>
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/*/*/*[not(*) and not(name() = 'note') and not(name() = value) and not(name() = 'country') and not(name() = 'p') and not(name() = 'link')]">
    <fo:block font-size="15pt" font-weight="normal" start-indent="35pt">
      <fo:inline font-weight="bold" font-size="15pt">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
        :
      </fo:inline>
      <xsl:value-of select="." />
      <xsl:if test="@year">
        (
        <xsl:value-of select="@year" />
        )
      </xsl:if>
      <xsl:if test="@height">
        (
        <xsl:value-of select="@height" />
        )
      </xsl:if>
      <xsl:if test="@worldComparison">
        <fo:inline font-weight="lighter" font-size="10pt">
          World comparison:
          <xsl:value-of select="@worldComparison" />
        </fo:inline>
      </xsl:if>
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/*/*/*/*[not(*) and not(name() = 'note') and not(name() = value) and not(name() = 'country') and not(name() = 'p') and not(name() = 'link')]">
    <fo:block font-size="15pt" font-weight="normal" start-indent="42pt">
      <fo:inline font-weight="bold" font-size="15pt">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
        :
      </fo:inline>
      <xsl:value-of select="." />
      <xsl:if test="@year">
        (
        <xsl:value-of select="@year" />
        )
      </xsl:if>
      <xsl:if test="@height">
        (
        <xsl:value-of select="@height" />
        )
      </xsl:if>
      <xsl:if test="@worldComparison">
        <fo:inline font-weight="lighter" font-size="10pt">
          World comparison:
          <xsl:value-of select="@worldComparison" />
        </fo:inline>
      </xsl:if>
    </fo:block>
  </xsl:template>

  <xsl:template match="entity/*/*/*/*/*[not(*) and not(name() = 'note') and not(name() = value) and not(name() = 'country') and not(name() = 'p') and not(name() = 'link')]">
    <fo:block font-size="15pt" font-weight="normal" start-indent="49pt">
      <fo:inline font-weight="bold" font-size="15pt">
        <xsl:call-template name="breakIntoWords">
          <xsl:with-param name="string" select="name()" />
        </xsl:call-template>
        :
      </fo:inline>
      <xsl:value-of select="." />
      <xsl:if test="@year">
        (
        <xsl:value-of select="@year" />
        )
      </xsl:if>
      <xsl:if test="@height">
        (
        <xsl:value-of select="@height" />
        )
      </xsl:if>
      <xsl:if test="@worldComparison">
        <fo:inline font-weight="lighter" font-size="10pt">
          World comparison:
          <xsl:value-of select="@worldComparison" />
        </fo:inline>
      </xsl:if>
    </fo:block>
  </xsl:template>

  <xsl:template match="values" name="values">
    <fo:list-block start-indent="30pt" font-size="15pt" font-weight="normal">
      <xsl:for-each select="value[@year]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
              (
              <xsl:value-of select="@year" />
              )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[@country]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
              (
              <xsl:value-of select="@country" />
              )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[@date]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
              (
              <xsl:value-of select="@date" />
              )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[@lenght]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <fo:inline font-weight="bold">
                <xsl:value-of select="@lenght" />
                :
              </fo:inline>
              <xsl:value-of select="." />
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[@passengers]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
              (
              <xsl:value-of select="@passengers" />
              )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[@percentage]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
              (
              <xsl:value-of select="@percentage" />
              %
                )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[@name and @year]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <fo:inline font-weight="bold">
                <xsl:value-of select="@name" />
                :
              </fo:inline>

              <xsl:value-of select="." />
              (
              <xsl:value-of select="@year" />
              )
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
      <xsl:for-each select="value[not(@name or @year or @percentage or @passengers or @lenght or @country or @date)]">
        <fo:list-item>
          <fo:list-item-label end-indent="label-end()">
            <fo:block>-</fo:block>
          </fo:list-item-label>
          <fo:list-item-body start-indent="body-start()">
            <fo:block>
              <xsl:value-of select="." />
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
    </fo:list-block>
  </xsl:template>

  <xsl:template name="breakIntoWords">
    <xsl:param name="string" />
    <xsl:choose>
      <xsl:when test="string-length($string) &lt; 5">
        <xsl:value-of select="$string" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="breakIntoWordsHelper">
          <xsl:with-param name="string" select="$string" />
          <xsl:with-param name="token" select="substring($string, 1, 1)" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="breakIntoWordsHelper">
    <xsl:param name="string" select="''" />
    <xsl:param name="token" select="''" />
    <xsl:choose>
      <xsl:when test="string-length($string) = 0" />
      <xsl:when test="string-length($token) = 0" />
      <xsl:when test="string-length($string) = string-length($token)">
        <xsl:value-of select="$token" />
      </xsl:when>
      <xsl:when test="contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ',substring($string, string-length($token) + 1, 1))">
        <xsl:value-of select="concat($token, ' ')" />
        <xsl:call-template name="breakIntoWordsHelper">
          <xsl:with-param name="string" select="substring-after($string, $token)" />
          <xsl:with-param name="token" select="substring($string, string-length($token), 1)" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="breakIntoWordsHelper">
          <xsl:with-param name="string" select="$string" />
          <xsl:with-param name="token" select="substring($string, 1, string-length($token) + 1)" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>