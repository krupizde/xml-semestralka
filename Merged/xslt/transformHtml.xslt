<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:import href="html.xslt" />
    <xsl:output method="html" />
    <xsl:template match="/">
        <xsl:result-document href="html/index.html" method="html">
            <html>
                <head>
                    <meta charset="UTF-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <link rel="stylesheet" href="stylesIndex.css" />
                    <title>
                    Index page
                </title>
                </head>
                <body>
                    <nav>
                        <ul>
                            <xsl:for-each select="entities/*">
                                <li>
                                    <a href="{./name}.html">
                                        <xsl:value-of select="./name"></xsl:value-of>
                                    </a>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </nav>
                </body>
            </html>
        </xsl:result-document>
        <xsl:apply-templates select="*" />
    </xsl:template>

</xsl:stylesheet>
