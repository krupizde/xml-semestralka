## Popis

V projektu jsem zpracoval oblasti Afganistan, Switzerland, United states a Atlantic Ocean.Nejprve jsem si vytvořil
obecnou strukturu pro data - schema.xml, kde jsem zahrnul sjednocení informací z jednotlivých oblastí. Každá oblast poté
obsahuje jenom podmnožinu dat z tohoto obecného dokumentu. Data pro jednotlivé oblasti jsem vkládal ručně. Mám pocit, že
budu muset vyměnit klávesy CTRL, C a V. Proto jsou pro jednotlivé oblasti zpracovaná pouze data ze sekcí Introduction,
Geography, Environment, Government, Economy a Transportation.

Úplně jsem nepochopil v zadání část o spojení dokumentů pomocí DTD (asi mám dlouhé vedení), tak jsem vytvořil 2
varianty.

## První varianta

Ve složce "Separate data" jsou jednotlivé oblasti samostatně, každá ve vlastním souboru spolu s DTD validačním
schématem. Toto schéma je stejné pro všechny tyto oblasti. Ve složce "Separate data/schema" se nachází RelaxNG validační
schéma. Jelikož jsem obě schémata psal ručně, tak jsem RelaxNG psal v kompaktní formě (schema.rnc) a to poté pomocí
nástroje https://github.com/djc/rnc2rng převedl na xml variantu (schema.rng). Ve složce "Separate data/xslt" se nachází
předpisy pro XSL transformace. <br>
HTML: Potřebný nástroj: Saxon. Ve složce "Separate data" zavolat
příkaz `java -jar saxon.jar -s:<oblast>.xml -xsl:./xslt/html.xslt`, což ve složce
"Separate data/html" vytvoří odpovídající html výstupy pro danou oblast. Ze složky html nemazat soubor styles.css!!!!.
Soubor se negeneruje sám a obsahuje styly pro daný html dokument. Nechtěl jsem generovat styly přímo v xslt, proto jsem
se rozhodl pro tento přístup.<br>
PDF: Potřebný nástroj: Saxon, Fop. Ve složce "Separate data" zavolat
příkaz `java -jar saxon.jar -s:<oblast>.xml -xsl:./xslt/xslfo.xslt -o:dokument.fo`. poté zavolat
příkaz `java -jar fop.jar -fo dokument.fo -pdf dokument.pdf`. První příkaz pomocí xslt vygeneruje xslfo a druhý příkaz z
xslfo vygeneruje výstupní pdf.

## Druhá varianta

Ve složce Merged se nachází soubor "Data.xml", který obsahuje všechny spojené oblasti do jednoho souboru. Jinak pro tuto
variantu platí to samé, jako pro předchozí. Generování výstupů vypadá následovně:<br>
HTML: Potřebný nástroj: Saxon. Ve složce "Merged" zavolat
příkaz `java -jar saxon.jar -s:Data.xml -xsl:./xslt/transformHtml.xslt`, což ve složce
"Merged/html" vytvoří odpovídající html výstupy pro všechny oblasti + soubor "index.html" obsahující rozcestník na dané
oblasti . Ze složky html nemazat soubory styles.css a stylesIndex.css!!!!. Soubory se negenerují samy a obsahuje styly
pro dané html dokumenty. Nechtěl jsem generovat styly přímo v xslt, proto jsem se rozhodl pro tento přístup.<br>
PDF: Potřebný nástroj: Saxon, Fop. Ve složce "Separate data" zavolat
příkaz `java -jar saxon.jar -s:Data.xml -xsl:./xslt/xslfo.xslt -o:dokument.fo`. poté zavolat
příkaz `java -jar fop.jar -fo dokument.fo -pdf dokument.pdf`. První příkaz pomocí xslt vygeneruje xslfo a druhý příkaz z
xslfo vygeneruje výstupní pdf.

## XSLT

V xslt souborech se nachází Template pro rozdělení camel-case textu na jednotlivá slova. Tento template není mým
výtvorem, veškerý credit patří autorovi
https://stackoverflow.com/questions/1566540/xslt-add-space-after-lowercase-followed-by-uppercase-letter/8541650. <br>
V XSLT dokumentech jsem trochu zazmatkoval. Vytvořil jsem je komplikovanější, než by měli být a spoustu Template jsem definoval
zbytečně tam, kde by šlo použít obecný předpis. HTMl transformace a PDF transformace jsou si velmi podobné (
prakticky identické v počtu templatu), protože jsem prakticky překopíroval tyto Template a změnil jejich obsah.

## Jmenné prostrory

V xml souborech jsem nevyužil jmenné prostory (kromě xslt tedy, kde jsou potřeba). Důvod je ten, že jsem v tomto případě
neviděl důvod. Většina elementů má unikátní název, tedy nedochází ke kolizím. Pokud bych je skutečně využil, bylo by to
například pro rozdělení elementů "values", ale poté bych deklaroval jmenný prostor vždy pouze kvůli jednomu elementu,
což mi přišlo zbytečné.

Když už jsem se zmínil o elementu "values", uvědomuji si, že to není nejvhodnější způsob ukládání seznamů v mé
struktuře. Tento element musí ve validačním schématu mít velké množství nepovinných atributů zbytečně kvůli mámu návrhu
a v xslt souborech pro tento element musel vzniknout velký složitý předpis. Důvod tohoto mého rozhodnutí je jednoduchý -
chtěl jsem si ulehčit práci. Ano vím, není to úplně správné a ve výsledku se mi to vrátilo právě na zmíněném XSLT.
